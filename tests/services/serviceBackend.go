package services

import (
	"fmt"
	"testing"
	"time"

	"github.com/gruntwork-io/terratest/modules/k8s"
)

// status
var WasServiceBackendCheck bool = false

func BackendCheck(releaseName string, kubectlOptions *k8s.KubectlOptions) func(t *testing.T) {
	return func(t *testing.T) {
		backServiceName := fmt.Sprintf("back-end-service-%s", releaseName)

		k8s.WaitUntilServiceAvailable(t, kubectlOptions, backServiceName, 10, 5*time.Second)

		WasServiceBackendCheck = true
	}
}
