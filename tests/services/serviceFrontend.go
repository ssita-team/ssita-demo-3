package services

import (
	"fmt"
	"testing"
	"time"

	"github.com/gruntwork-io/terratest/modules/k8s"
)

// status
var WasServiceFrontendCheck bool = false

func FrontendCheck(releaseName string, kubectlOptions *k8s.KubectlOptions) func(t *testing.T) {
	return func(t *testing.T) {
		frontServiceName := fmt.Sprintf("front-end-service-%s", releaseName)

		k8s.WaitUntilServiceAvailable(t, kubectlOptions, frontServiceName, 10, 5*time.Second)

		WasServiceFrontendCheck = true
	}
}
