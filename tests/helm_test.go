package test

import (
	"fmt"
	"os"
	"strings"
	"testing"

	"tests/backend"
	"tests/frontend"
	"tests/helper"
	"tests/ingress"
	"tests/services"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/gruntwork-io/terratest/modules/k8s"
	"github.com/gruntwork-io/terratest/modules/random"
)

// input variables
// For production you should use variables
var namespaceName string = "test-terratest"
var kubectlOptions *k8s.KubectlOptions = k8s.NewKubectlOptions("", "", namespaceName)
var options *helm.Options = &helm.Options{
	KubectlOptions: kubectlOptions,
	SetValues: map[string]string{
		"container_front":     os.Getenv("FRONT_TEST_IMAGE"),
		"container_back":      os.Getenv("BACK_TEST_IMAGE"),
		"JWT_SECRET":          os.Getenv("JWT_SECRET"),
		"DATASOURCE_URL":      os.Getenv("DATASOURCE_URL"),
		"DATASOURCE_USER":     os.Getenv("DATASOURCE_USER"),
		"DATASOURCE_PASSWORD": os.Getenv("DATASOURCE_PASSWORD"),
		"front_dns":           os.Getenv("front_dns"),
		"back_dns":            os.Getenv("back_dns"),
	},
}
var releaseName string = fmt.Sprintf(
	"app%s",
	strings.ToLower(random.UniqueId()),
)

// Put your tests here then they will execute as subtests
func TestKubernetes(t *testing.T) {

	// Destroy relesae after testing
	defer helper.Destroy(t, releaseName, options)

	helper.Deploy(t, releaseName, "../helm", options)

	// Check if deployment was successful
	if !helper.WasDeploySuccessful {
		t.Fatalf("Deploy was failed")
	}

	// Run tests
	t.Run("ServiceBackend", services.BackendCheck(releaseName, kubectlOptions))
	t.Run("serviceFrontend", services.FrontendCheck(releaseName, kubectlOptions))
	t.Run("Ingress", ingress.IngressCheck(releaseName, kubectlOptions))
	t.Run("AvailabilityBackend", backend.AvailabilityBackend(releaseName, kubectlOptions))
	t.Run("AvailabilityFrontend", frontend.AvailabilityFrontend(releaseName, kubectlOptions))
}
