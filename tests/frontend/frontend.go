package frontend

import (
	"fmt"
	"testing"
	"tests/helper"
	"tests/services"

	"github.com/gruntwork-io/terratest/modules/k8s"
)

func AvailabilityFrontend(releaseName string, kubectlOptions *k8s.KubectlOptions) func(t *testing.T) {
	return func(t *testing.T) {

		// Checking was Backend service successfully deployed
		if !services.WasServiceFrontendCheck {
			t.Fatalf("Service frontend check was failed")
		}

		frontServiceName := fmt.Sprintf("front-end-service-%s", releaseName)
		frontService := k8s.GetService(t, kubectlOptions, frontServiceName)

		frontUrl := fmt.Sprintf("http://%s/dev", k8s.GetServiceEndpoint(t, kubectlOptions, frontService, 80))

		helper.Verify(t, 200, frontUrl, "html", 10)
	}
}
