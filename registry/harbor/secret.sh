openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -out tls.crt \
    -keyout tls.key \
    -subj "/CN=harbor.ssita-demo.buzz/O=harbor-ingress-tls"

kubectl create secret tls harbor-ingress-tls \
    --namespace ingress-nginx \
    --key tls.key \
    --cert tls.crt