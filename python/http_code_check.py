import requests
import time
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError

def url_code_status(url):
    req = Request(url)
    try:
        response = urlopen(req)
        return response.getcode()
    except HTTPError as e:
        print('The server couldn\'t fulfill the request.')
        print('Error code: ', e.code)
        return e.code
    except URLError as e:
        print('We failed to reach a server.')
        print('Reason: ', e.reason)
        return e.reason

def check_from_file():
    file = open("/builds/ssita-team/ssita-demo-3/python/urls.txt", "r")
    file_lines = file.read()
    url_list = file_lines.split("\n")

    for url in url_list:
        try:
            print(url + ' ---> ' + str(url_code_status(url)))
            assert url_code_status(url) == 200
            time.sleep(1)
        except:
            print("Warning: NOT 200")

check_from_file()
