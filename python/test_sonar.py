import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import os

option = webdriver.ChromeOptions()
option.add_argument('--no-sandbox')
option.add_argument('--headless')

class SonarTestCase(unittest.TestCase):
    
    expected = ["dev", "ssita-demo-3", "teachua-front"]
    
    def setUp(self):
        self.browser = webdriver.Chrome(options=option)
        self.addCleanup(self.browser.quit)

    def test_sonar_title(self): 
        self.browser.get(
            'https://sonar.ssita-demo.buzz')
        self.assertIn('Sonar', self.browser.title)
        
    def test_sonar_page(self):
        
        password = os.environ["SONAR_PASSWORD"]
        
        self.browser.get(
            'https://sonar.ssita-demo.buzz')
        
        time.sleep(5)
        
        login = self.browser.find_element(
            by=By.CLASS_NAME, value="login-input")
        login.send_keys("Bohdan")
        
        passw = self.browser.find_element(by=By.ID, value='password')
        passw.send_keys(password)
        
        button = self.browser.find_element(
            by=By.CLASS_NAME, value='button').click()
        
        time.sleep(5)
    
        total = int(self.browser.find_element(
            by=By.ID, value='projects-total').text)
        self.assertEqual(len(SonarTestCase.expected), total)
        
        for ex in SonarTestCase.expected:
            total = self.browser.find_element(by=By.LINK_TEXT, value=ex).text
            self.assertEqual(ex, total)
            print("ok " + ex)


if __name__ == "__main__":
    unittest.main(verbosity=2)
