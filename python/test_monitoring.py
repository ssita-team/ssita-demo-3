import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import os

option = webdriver.ChromeOptions()
option.add_argument('--no-sandbox')
option.add_argument('--headless')

class MonitorTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(options=option)
        self.addCleanup(self.browser.quit)

    def test_grafana_title(self):
        self.browser.get(
            'https://grafana.ssita-demo.buzz')
        self.assertIn('Grafana', self.browser.title)

    def test_grafana_page(self):
        
        password = os.environ["GRAFANA_PASSWORD"]

        self.browser.get(
            'https://grafana.ssita-demo.buzz')

        time.sleep(5)

        login = self.browser.find_element(
            by=By.NAME, value="user")
        login.send_keys("admin")

        passw = self.browser.find_element(
            by=By.NAME, value='password')
        passw.send_keys(password)

        button = self.browser.find_element(
            by=By.CLASS_NAME, value='css-1s5ybvs-button').click()
        
        time.sleep(5)
        
        page = self.browser.find_element(
            by=By.CLASS_NAME, value='css-1aanzv4').text
        self.assertEqual('Welcome to Grafana', page)

if __name__ == '__main__':
    unittest.main(verbosity=2)
