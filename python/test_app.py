import os
from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time
import re

option = webdriver.ChromeOptions()
option.add_argument('--no-sandbox')
option.add_argument('--headless')

class ApplicationTestCase(unittest.TestCase):
    
    def test_app_page(self):
        
        expected = "Львів"

        driver = webdriver.Chrome(options=option)
        driver.get("https://front.ssita-demo.buzz/dev/")
        
        time.sleep(2)
        
        driver.find_element(by=By.CSS_SELECTOR, value=".anticon.anticon-caret-down").click()
        
        time.sleep(2)
        
        city = driver.find_element(
            by=By.XPATH, value="//ul[@role='menu']/li[8]/span[@class='ant-dropdown-menu-title-content']").text
        self.assertEqual(expected, city)

        driver.close()


if __name__ == "__main__":
    unittest.main(verbosity=2)
